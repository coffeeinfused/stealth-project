﻿using UnityEngine;
using System.Collections;

public class NPCAudio : MonoBehaviour {

    public AudioClip[] footSteps;
    public AudioClip[] surprised;
    public AudioClip hit;

    AudioSource audioPlayer;

    void Start() {
        audioPlayer = this.GetComponent<AudioSource>();
    }

    public void playFootstep() {
        int rndNo = Random.Range(0, footSteps.Length);
        audioPlayer.PlayOneShot(footSteps[rndNo]);
    }

    public void playSurprise() {
        int rndNo = Random.Range(0,surprised.Length);
        audioPlayer.PlayOneShot(surprised[rndNo]);
    }

    public void playHit() {
        audioPlayer.PlayOneShot(hit);
    }
}
