﻿using UnityEngine;
using System.Collections;

public class pickUpObj : MonoBehaviour {
    GameObject gm;

    void Start() {
        gm = GameObject.Find("Game Controller");
    }

    void OnTriggerEnter(Collider col) {

        if (col.gameObject.tag == "Player")
        {
            gm.GetComponent<gameManager>().collectItem();
            this.gameObject.SetActive(false);
        }
    }
}
