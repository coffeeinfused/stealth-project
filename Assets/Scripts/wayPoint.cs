﻿using UnityEngine;
using System.Collections;

public class wayPoint : MonoBehaviour {

    void OnTriggerEnter(Collider col) {
        if (col.gameObject.tag == "enemyGuard")
        {
            col.gameObject.GetComponent<guardMotion>().nextWaypoint();

            Debug.Log(col.gameObject.name + " arrived at " + this.gameObject.name);
        }
    }
}
