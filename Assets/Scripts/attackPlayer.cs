﻿using UnityEngine;
using System.Collections;

public class attackPlayer : MonoBehaviour {

    bool linedUp = false;
    guardMotion controller;
    //NavMeshAgent thisAgent;
    //float AgentSpeed;

    void Start() {
        controller = this.GetComponentInParent<guardMotion>();
        //thisAgent = this.transform.root.GetComponent<NavMeshAgent>();
    }

void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            linedUp = true;
            Invoke("throwPunch", controller.reactDelay);
            //AgentSpeed = thisAgent.speed;
            //thisAgent.speed = 0;
            //thisAgent.Stop();
        }
    }

    void OnTriggerExit(Collider col) {
        if (col.gameObject.tag == "Player") {
            linedUp = false;
            controller.thisAnim.SetBool("PunchBool", false);
            //thisAgent.speed = AgentSpeed;
            //thisAgent.Resume();
        }
    }

    void throwPunch() {
        controller.thisAnim.SetBool("PunchBool", true);
        controller.attack(linedUp);
    }


}
