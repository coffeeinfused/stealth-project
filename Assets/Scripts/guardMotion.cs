﻿using UnityEngine;
using System.Collections;

public class guardMotion : MonoBehaviour {

    public NavMeshAgent thisAgent;
    public Animator thisAnim;

    public bool cyclingPath = true;
    public Transform[] wayPoints;
    public float reactDelay = 1.0f;
    public float enemySpeed = 1.0f;
    public Transform radarPoint;
    public float distRange = 100.0f;
    public float tooFarReact = 5.0f;
    public float patrolTime = 30.0f;

    float AgentSpeed;

    GameObject gm;

    GameObject player;

    Vector3 targetLocation;

    Vector3 distanceAway = new Vector3(0.0f, 0.0f, 2.50f);

    int waypointNo = 0;

	// Use this for initialization
	void Start () {
        thisAgent = this.GetComponent<NavMeshAgent>();
        thisAnim = this.GetComponentInChildren<Animator>();
        AgentSpeed = thisAgent.speed;
        gm = GameObject.FindGameObjectWithTag("GameController");

        player = GameObject.FindGameObjectWithTag("Player");

        waypointNo = 0;
        loopWaypoints();
        resetPatrol();
    }

    void resetPatrol()
    {
        cyclingPath = true;
        InvokeRepeating("spotPlayer", 0.0f, 0.01f);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log(this.name + " bumped into the Player");
            StartCoroutine(bumpIntoPlayer());
            AgentSpeed = thisAgent.speed;
            thisAgent.speed = 0;
            thisAgent.Stop();
        }
    }

    /*void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            thisAgent.speed = AgentSpeed;
            thisAgent.Resume();
        }
    }*/

    public void attack(bool linedUp)
    {
        if (linedUp)
        {
            gm.GetComponent<gameManager>().deductOne();
            Debug.Log(this.name + " hit the Player");
        }
    }

    void spotPlayer()
    {
        RaycastHit objInfo;

        //Vector3 euler = radarPoint.transform.eulerAngles;

        //Vector3 point = genVisiblePoint();

        //Vector3 randomDirection = radarPoint.transform.forward + (Random.insideUnitSphere * distRange);
        //Vector3 randomDirection = (Random.insideUnitSphere * distRange);

        //if (Physics.Raycast(radarPoint.transform.position, radarPoint.transform.forward + randomDirection, out objInfo, distRange))
        //if (Physics.Raycast(radarPoint.transform.position, radarPoint.forward, out objInfo, distRange))

        float newAngle = Random.Range(0f, 360f);
        Vector3 visX = new Vector3(Mathf.Sin(Mathf.Deg2Rad * newAngle), 0.0f, 0.0f);

        /*Vector3 targetDir = radarPoint.position - transform.position;
        Vector3 forward = radarPoint.transform.forward;*/

        if (Physics.Raycast(radarPoint.transform.position, radarPoint.transform.forward + visX, out objInfo, distRange))
        {
            Debug.DrawRay(radarPoint.transform.position, radarPoint.transform.forward + visX, Color.red, 0.5f);
            GameObject potentialTarget = objInfo.collider.gameObject;

            if (potentialTarget.tag == "Player")
            {
                if (rangeToPlayerCheck() == true)
                {
                    Debug.Log(this.name + " spotted the Player");
                    StartCoroutine(bumpIntoPlayer());
                }

                if (rangeToPlayerCheck() != true)
                {
                    Debug.Log(this.name + " is too far from the Player");
                }
            }
        }

    }

    /*Vector3 genVisiblePoint() {
        float angle = 180.0f;
        Vector3 randomDirection = new Vector3(0.0f, 0.0f, 0.0f);

        while (angle < -30.0f & angle > 30.0f)
        {
            randomDirection = radarPoint.transform.forward + (Random.insideUnitSphere * distRange);
            angle = Vector3.Angle(randomDirection, radarPoint.forward);
            Debug.Log(randomDirection.ToString());
        }
        return randomDirection;
    }*/


    IEnumerator bumpIntoPlayer()
    {
        thisAnim.SetBool("Walk Forward", false);

        //thisAgent.SetDestination(this.transform.position);

        this.gameObject.transform.LookAt(player.transform.position);

        //this.transform.LookAt(newTarget);
        yield return new WaitForSeconds(reactDelay);
        speedUp();
    }

    void speedUp() {
        CancelInvoke("spotPlayer");
        cyclingPath = false;
        thisAnim.SetBool("Walk Forward", true);

        thisAgent.speed = 10.0f;
        thisAnim.speed = 2.0f;

        InvokeRepeating("chase", 0.25f, 0.5f);
    }


    void chase()
    {

        Debug.Log(this.name + " is chasing the Player");

        cyclingPath = false;
       
        bool chasing = rangeToPlayerCheck();

        if (chasing)
        {
            this.thisAgent.Resume();
            thisAgent.SetDestination(player.transform.position + distanceAway);
            InvokeRepeating("turnToPlayer", 0.0f, 0.125f);
        } else
        {
            stopChase();
        }
    }

    void turnToPlayer() {
        if (this.transform.position == player.transform.position + distanceAway)
        {
            this.transform.LookAt(player.transform.position);
        }
    }

    void stopChase()
    {
        thisAgent.speed = 3.5f;
        thisAnim.speed = 1.0f;

        CancelInvoke("chase");

        Debug.Log(this.name + " stopped chasing the Player");

        CancelInvoke("turnToPlayer");

        cyclingPath = true;
        loopWaypoints();
        resetPatrol();
    }

    bool rangeToPlayerCheck()
    {
        if (Vector3.Distance(this.transform.position, player.transform.position) > tooFarReact)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void nextWaypoint() {
        //thisAgent.Stop();
        //thisAgent.destination = this.transform.position;

        waypointNo++;

        if (waypointNo >= wayPoints.Length)
        {
            waypointNo = 0;
        }
        loopWaypoints();
    }

    public void loopWaypoints()
    {
        if (cyclingPath)
        {
            thisAgent.speed = AgentSpeed;
            thisAgent.Resume();
            thisAgent.SetDestination(wayPoints[waypointNo].position);
        }
    }
}
