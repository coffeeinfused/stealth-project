﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{

    public int hitCount = 5;
    public int collectCount;
    
    GameObject[] collectables;

    GameObject winScreen;
    GameObject deathScreen;

    Text collectNum;
    Text healthNum;

    // Use this for initialization
    void Start () {
        collectables = GameObject.FindGameObjectsWithTag("pickUp");
        collectCount = collectables.Length;

        winScreen = GameObject.Find("youWon");
        deathScreen = GameObject.Find("youDied");
        collectNum = GameObject.Find("collectNum").GetComponent<Text>();
        healthNum = GameObject.Find("healthNum").GetComponent<Text>();

        displayCount();
        displayHealth();
        winScreen.SetActive(false);
        deathScreen.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	}

    public void collectItem()
    {
        collectCount--;

        displayCount();

        if (collectCount <= 0) {
            winScreen.SetActive(true);
            StartCoroutine("resetLevel");
        }
    }

    void displayHealth() {
        healthNum.text = hitCount + " / 5";
    }

    void displayCount()
    {
        collectNum.text = collectCount + " / " + collectables.Length;
    }

    public void deductOne()
    {
        hitCount--;
        displayHealth();
        if (hitCount <= 0)
        {
            deathScreen.SetActive(true);
            StartCoroutine("resetLevel");
        }
    }

    IEnumerator resetLevel()
    {
        yield return new WaitForSeconds(7.5f);
        Application.LoadLevel(Application.loadedLevel);
    }
}
