﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;


public class reduceRunSpeed : MonoBehaviour {

    public float runSpeed;

    public float dropAmount = 0.25f;

    bool running;
    FirstPersonController firstPersonCont;


	// Use this for initialization
	void Start () {
        firstPersonCont = this.GetComponent<FirstPersonController>();
        runSpeed = firstPersonCont.m_RunSpeed;
	}
	
	// Update is called once per frame
	void Update () {
	if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            running = true;
            runSpeed = firstPersonCont.m_RunSpeed;
            CancelInvoke("increaseSpeed");
            InvokeRepeating("dropSpeed", 0.0f, 0.5f);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            running = false;
            CancelInvoke("dropSpeed");
            InvokeRepeating("increaseSpeed", 0.0f, 0.25f);
        }
    }

    void dropSpeed() {
        firstPersonCont.m_RunSpeed -= dropAmount;

        if (firstPersonCont.m_RunSpeed < (runSpeed * 0.15f))
        {
            CancelInvoke("dropSpeed");
        }
    }

    void increaseSpeed()
    {
        firstPersonCont.m_RunSpeed += dropAmount;

        if (firstPersonCont.m_RunSpeed == runSpeed)
        {
            CancelInvoke("increaseSpeed");
        }
    }
}
